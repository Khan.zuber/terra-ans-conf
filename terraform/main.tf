terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = "us-east-1"
}

resource "aws_security_group" "instance" {
  name = "terraform-ansible-instance"

  ingress {
    from_port   = 0
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  ami           = "ami-08d4ac5b634553e16"
  instance_type = "t2.micro"
  key_name      = "mykey.pem"
  vpc_security_group_ids = [aws_security_group.instance.id]
  count         = "1"
  tags = {
    Name = "ansible-master"
  }


provisioner "local-exec" {
  command = <<-EOT
    echo '[ansible]' >> inventory
    echo 'ansible-master ansible_host=${aws_instance.example.private_ip} ansible_connection=local' >> inventory
    echo '[nodes]' >> inventory
    echo 'node1 ansible_host=${aws_instance.slave-ec2-instance[0].private_ip} ansible_connection=local' >> inventory
    echo 'node2 ansible_host=${aws_instance.slave-ec2-instance[1].private_ip} ansible_connection=local' >> inventory
  EOT
}

provisioner "file" {
  source      = "inventory"
  destination = "/home/ubuntu/inventory"
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/zuber/.ssh/mykey.pem")
    host        = self.public_ip
  }
}

provisioner "local-exec" {
  command = <<-EOT
      rm -rf inventory
      EOT
  when    = destroy
}

provisioner "local-exec" {
  command = <<-EOT
      rm -rf inventory
      EOT
  when    = destroy
}

provisioner "remote-exec" {
    inline = ["echo 'Wait until SSH is ready'"]
}

provisioner "remote-exec" {
  inline = [
    "sleep 120; ansible-playbook engine-config.yaml"
  ]
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/zuber/.ssh/mykey.pem")
      host        = aws_instance.example.public_ip
  }
}
}

resource "aws_instance" "slave-ec2-instance" {
  ami                    = "ami-08d4ac5b634553e16"
  instance_type          = "t2.micro"
  key_name               = "mykey.pem"
  count                  = "2"
  user_data              = file("user-data-ansible-node.sh")
  vpc_security_group_ids = [ aws_security_group.instance.id ]
}
output "address" {
  value = "${aws_instance.example.*.public_dns}"
}